%% SST - projekt, KRʯEL, KAMI�SKI
clear;close;clc;
disp('START-------------------------------------------------------------');
disp('test.m');

%% DANE
%dane historyczne z MetaTrader'a
dane = load('EURUSD240.csv','-ascii');
probka  = size(dane); probka  = probka(1); 
probka  = 1:probka;  %liczba pr�bek do wyswietlenia na osi X, daty jako� dziwnie eksportowa�o z MT do pliku, dlatego tak to jest zrobione

data    = dane(:,1); %to jako� dziwnie wczytuje, dlatego zamiast daty na osi x jest liczba pr�bek (rozmiar macierzy)
godzina = dane(:,2); %to niepotrzebne
otworz  = dane(:,3);
high    = dane(:,4);
low     = dane(:,5);
zamknij = dane(:,6);
wolumen = dane(:,7);

%% WYKRESY
figure(1); 
subplot(2,1,1);    
    plot(probka,zamknij,'b'); 
    hold on;
    title('EUR / USD'); xlabel('numer pr�bki'); ylabel('zamkni�cie pozycji');
subplot(2,1,2);
    plot(probka,wolumen,'r');
    title('EUR /USD'); xlabel('numer pr�bki'); ylabel('wolumen');
    

%% MODELE
%strktura utworzona z danych z MetaTrader'a
data = iddata(zamknij,wolumen,1);

%ARX
disp('MODEL ARX --------------------------------------------------------');
ARX.na = 15;
ARX.nb = 15;
ARX.nk = 4;
ARX_model = arx(data,[ARX.na,ARX.nb,ARX.nk]);
disp('Stopie� dopasowania modelu ARX [%]:');
ARX_fit = ARX_model.Report.Fit.FitPercent; %stopie� dopasowania modelu
disp(ARX_fit);

%ARMAX
disp('MODEL ARMAX ------------------------------------------------------');
ARMAX.na = 10;
ARMAX.nb = 4;
ARMAX.nc = 2;
ARMAX.nk = 1;
ARMAX_model = armax(data,[ARMAX.na,ARMAX.nb,ARMAX.nc,ARMAX.nk]);
disp('Stopie� dopasowania modelu ARMAX [%]:');
ARMAX_fit = ARMAX_model.Report.Fit.FitPercent; %stopie� dopasowania modelu
disp(ARMAX_fit);

%OE
disp('MODEL OE ---------------------------------------------------------');
OE.nb = 5;
OE.nf = 5;
OE.nk = 4;
OE_model = oe(data,[OE.nb,OE.nf,OE.nk]);
disp('Stopie� dopasowania modelu OE [%]:');
OE_fit = OE_model.Report.Fit.FitPercent; %stopie� dopasowania modelu
disp(OE_fit);

%BOX-JENKINS
disp('MODEL B-J --------------------------------------------------------');
BJ.nb = 10;
BJ.nc = 3;
BJ.nd = 2;
BJ.nf = 4;
BJ.nk = 4;
BJ_model = bj(data,[BJ.nb,BJ.nc,BJ.nd,BJ.nf,BJ.nk]);
disp('Stopie� dopasowania modelu B-J [%]:');
BJ_fit = BJ_model.Report.Fit.FitPercent; %stopie� dopasowania modelu
disp(BJ_fit);

%HAMMERSTEIN-WIENER
disp('MODEL H-W --------------------------------------------------------');
HW.nb = 10;
HW.nf = 5;
HW.nk = 4;
HW_model = nlhw(data,[HW.nb,HW.nf,HW.nk]);
disp('Stopie� dopasowania modelu H-W [%]:');
HW_fit = HW_model.Report.Fit.FitPercent; %stopie� dopasowania modelu
disp(HW_fit);



disp('KONIEC------------------------------------------------------------');
